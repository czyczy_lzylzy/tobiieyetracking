import cv2
import numpy as np
import global_setting as glb

def getPtlist(im):
    wname = 'SetAOI'
    pt = []
    im_aoi = np.copy(im)
    cv2.namedWindow(wname)

    cv2.setMouseCallback(wname, addPoints, [wname, im_aoi, pt])
    cv2.imshow(wname, im_aoi)
    cv2.waitKey()
    cv2.destroyAllWindows()
    im_aoi = cv2.polylines(im, np.int32([pt]), True, (255,255,0))
    cv2.imshow(wname, im_aoi)
    cv2.waitKey(0)
    return pt, im_aoi


def getArea(im, win_name, matches, i_kp):
    im_area = np.copy(im)
    addTokuchoten(im_area, win_name, matches, i_kp)
    glb.points = []
    cv2.namedWindow(win_name)
    glb.im_area = np.copy(im_area)
    cv2.setMouseCallback(win_name, dragArea, win_name)
    cv2.imshow(win_name, glb.im_area)
    cv2.waitKey()


def addTokuchoten(im, win_name, matches, i_kp):
    ratio = 0.7
    i_match = []

    ## レシオテストと範囲指定
    for m, n in matches:
        if m.distance < ratio * n.distance:
            i_point = list((np.array(i_kp[m.trainIdx].pt)*0.5).astype(np.int))
            i_match.append(i_point)

    if len(i_match) != 0:
        for p in i_match:
            im = cv2.circle(im, tuple(p), 2, [15, 241, 255], thickness=-1)
    


def addPoints(event, x, y, flag, params):
    wname, img, ptlist = params

    if event == cv2.EVENT_MOUSEMOVE:  
        img2 = np.copy(img)
        h, w = img2.shape[0], img2.shape[1]
        cv2.line(img2, (x, 0), (x, h - 1), (255, 0, 0))
        cv2.line(img2, (0, y), (w - 1, y), (255, 0, 0))
        cv2.imshow(wname, img2)

    if event == cv2.EVENT_LBUTTONDOWN:
        ptlist.append([x, y])
        # print('[{}] ( {}, {} )'.format(len(ptlist), x, y))
        cv2.circle(img, (x, y), 3, (0, 0, 255), 3)
        cv2.imshow(wname, img)

        if cv2.waitKey(1) == '\n':
            cv2.destroyWindow(wname)

def dragArea(event, x, y, flag, params):
    wname = params

    if event == cv2.EVENT_MOUSEMOVE:
        if glb.drag:
            im_area = np.copy(glb.im_area)
            cv2.rectangle(im_area, tuple(glb.s_point_harf), (x,y), (255,0,0))
            cv2.imshow(wname, im_area)


    if event == cv2.EVENT_LBUTTONDOWN:
        if not glb.drag:
            glb.s_point = [x*2,y*2]
            glb.s_point_harf = [x,y]
            glb.drag = True
    
    if event == cv2.EVENT_LBUTTONUP:
        if glb.drag:
            glb.points.append([glb.s_point,[x*2,y*2]])
            cv2.rectangle(glb.im_area, tuple(glb.s_point_harf), (x,y), (255,0,0))
            glb.drag = False

    if cv2.waitKey(1) == '\n':
        cv2.destroyWindow(wname)
