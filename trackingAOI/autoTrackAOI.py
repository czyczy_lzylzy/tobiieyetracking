import cv2
import numpy as np
from numpy.lib.utils import _makenamedict
import pandas as pd
import sys
import itertools
import joblib
import mouseAuto as ms
import global_setting as glb
import aoiMod as amod
sys.path.append('mymodule')
import tobiiUnpack as tobii
import tarminal


def dataPreprocessing(data):
    if data is None:
        return
    time, aoi_name = data

    if aoi_name is None:
        if glb.prev_aoi != 'out':
            glb.export_data.append([glb.s_time/1000, time, glb.prev_aoi])
            glb.prev_aoi = 'out'
        
    elif aoi_name != glb.prev_aoi:
        if glb.prev_aoi != 'out':
            glb.export_data.append([glb.s_time/1000, time, glb.prev_aoi])
        glb.prev_aoi = aoi_name
        glb.s_time = time


def makeIm():
    ## AOI情報を追加した映像を表示
    test_im = np.copy(glb.frame)
    
    ## AOIの枠を画像に追加
    if aoi.v_points is not None:
        test_im = amod.exportAOI(aoi, cv2.polylines(test_im, np.int32([aoi.v_points]), True, (255,255,0)))
    
    ## トラッキングしている特徴点を画像に追加
    if aoi.track_points is not None:
        for p in aoi.track_points:
            test_im = cv2.circle(test_im, tuple(map(int, p)), 5, (0,255,0), thickness=-1)

    ## 注視点を画像に追加
    test_im = cv2.circle(test_im, eye.getSightPoint(glb.v_time_now), 10, (15,241,255))

    ## 経過時間を追加
    test_im = cv2.putText(test_im, str(glb.v_time_now), (0,50), cv2.FONT_HERSHEY_PLAIN, 3, (255, 255, 255), 5, cv2.LINE_AA)

    ## scoreを追加
    if aoi.best_M is not None:
        test_im = cv2.putText(test_im, str(aoi.best_M['score']), (0,100), cv2.FONT_HERSHEY_PLAIN, 3, (255, 255, 255), 5, cv2.LINE_AA)

    return test_im


if __name__ == '__main__':

    glb.init()
    glb.EXPORT_NAME = glb.EXPORT_NAME.replace('export/', 'export/auto_')

    ## 動画の情報を取得
    cap = cv2.VideoCapture(glb.DIR_PATH+'fullstream.mp4')

    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = float(cap.get(cv2.CAP_PROP_FPS))
    all_frame = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    ## 目標画像の情報を取得
    im = cv2.imread(glb.SEARCH_IM)

    video = amod.setVideoConfig(fps, width, height)
    aoi, im_aoi = amod.setAOI(im)
    amod.saveAOI(aoi, im_aoi)
    aoi.setImSize(width, height)
    mask, im_mask = amod.setMask(im)

    ## 視線情報ファイルの読み込み
    eye = tobii.Data(glb.DIR_PATH+'livedata.json.gz', width, height)
    eye.readLivedata()

    glb.prev_im = None

    p_bar = tarminal.Progbar(int(cap.get(cv2.CAP_PROP_FRAME_COUNT)), message='Start Encoding.')

    ## 1フレーム目の情報を取得
    ret, glb.frame = cap.read()
    glb.v_frame_now = cap.get(cv2.CAP_PROP_POS_FRAMES)
    glb.v_time_now = cap.get(cv2.CAP_PROP_POS_MSEC)

    height, width, _ =  glb.frame.shape
    aoi.setVideSize(width, height)


    while True:

        p_bar.view(glb.v_frame_now)

        if ret:

            ## AOIの検出、トラッキング
            if not aoi.inFrame:
                amod.aoiMatching(aoi, glb.frame, im, glb.detector, glb.matcher, ratio=glb.ratio)
            else:
                sucsess = aoi.tracking(glb.prev_im, glb.frame)

                if not sucsess: ## トラッキング失敗
                    amod.aoiMatching(aoi, glb.frame, im, glb.detector, glb.matcher, ratio=glb.ratio)
                elif aoi.best_M['score'] < 0.008: ## トラッキングしている点のスコアが低い場合
                    amod.updateMatching(aoi, glb.frame, im, glb.detector, glb.matcher, ratio=glb.ratio-0.15)

            if aoi.inFrame or (not aoi.inFrame and glb.prev_aoi != 'out'):
                ## AOI hitとフレーム画像をキューにプッシュ
                write_data = glb.aoi_q.push((glb.v_time_now, aoi.hitAOI(eye.getSightPoint(glb.v_time_now))))
                ## AOI hitデータの前処理
                dataPreprocessing(write_data)

            ## AOI情報を追加した映像を表示
            test_im = makeIm()

            glb.prev_im = glb.frame

            ## プレビュー
            # test_im = cv2.resize(test_im, (int(width*0.5),int(height*0.5)))
            # cv2.imshow(glb.WIN_NAME, test_im)

            write_im = glb.frame_q.push(test_im)

            ## フレーム画像を動画に出力
            if write_im is not None:
                video.write(write_im)
            
        
        else:
            break

        ret, glb.frame = cap.read()
        glb.v_frame_now = cap.get(cv2.CAP_PROP_POS_FRAMES)
        glb.v_time_now = cap.get(cv2.CAP_PROP_POS_MSEC)

    p_bar.progFinish('Finish Encoding.')
    
    ## 残りのデータを前処理
    for d in glb.aoi_q.q:
        dataPreprocessing(d)

    ## 残りのフレームを動画に出力
    for f in glb.frame_q.q:
        video.write(f)
        
    ## AOI hitをファイル出力
    df = pd.DataFrame(glb.export_data)
    df.to_csv('export/aoi_hit.csv', header=False, index=False)