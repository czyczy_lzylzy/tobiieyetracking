#!Python3.7
import numpy as np
import cv2
from numpy.lib import ufunclike
import screeninfo


## opencv-python 4.3.x


class Resize():
    
    def __init__(self, target_monitor=0) -> None:
        monitor = screeninfo.get_monitors()[target_monitor]
        self.monitor_w = monitor.width
        self.monitor_h = monitor.height
        self.mode = dict()

    def addResizeMode(self, name, im, screen_occupancy) -> None:
        bairitsu = self.calculateBairitsu(im, screen_occupancy)

        h, w = im.shape[:2]

        self.mode[name] = {
                            'baseShape':im.shape[:2], 
                            'occupancy':screen_occupancy,
                            'bairitsu':bairitsu,
                            'resizeShape':(int(w*bairitsu), int(h*bairitsu))
                        }

    def calculateBairitsu(self, im, screen_occupancy) -> float:
        re_im_h = self.monitor_h * screen_occupancy
        re_im_w = self.monitor_w * screen_occupancy
        h, w = im.shape[:2]

        if re_im_w > self.monitor_w:
            bairitsu = re_im_h / h
        else:
            bairitsu = re_im_w / w

        return bairitsu

    def toResizeFromBaseXY(self, mode, xy:list) -> float:
        return (np.array(xy) * self.mode[mode]['bairitsu']).astype(int).tolist()

    def toBaseFromResizeXY(self, mode, xy:list) -> float:
        return (np.array(xy) / self.mode[mode]['bairitsu']).astype(int).tolist()

    def resize(self, mode, im) -> any:
        if im.shape[:2] != self.mode[mode]['baseShape']:
            self.addResizeMode(im, mode, im, self.mode[mode]['occupancy'])

        return cv2.resize(im, self.mode[mode]['resizeShape'])

    def getBairitsu(self, mode) -> float:
        return self.mode[mode]['bairitsu']


if __name__ == '__main__':

    im = cv2.imread('sample.jpg')

    ## 表示する画像の画面占有率をセット
    resize = Resize()
    resize.addResizeMode('sample', im, 0.2)

    ## 画像をリサイズ
    im = resize.resize('sample', im)

    cv2.imshow('resize', im)

    cv2.waitKey(0)