import cv2
import numpy as np
import json
import gzip
import sys

class Data():

    def __init__(self, file_path, v_width=None, v_height=None) -> None:
        self.f_path = file_path
        self.v_width = v_width
        self.v_height = v_height
        self.gp_list = []
        self.ts_list = []

    def setWidthHeight(self, v_width, v_height) -> None:
        self.v_width = v_width
        self.v_height = v_height

    def getNearestIndex(self, lis, num):
        idx = np.abs(np.asarray(lis) - num).argmin()
        return idx
    

    def getSightPoint(self, v_time_now):
        time_micro = v_time_now * 1000
        now_listIdx = self.getNearestIndex(self.ts_list, time_micro)
        gp = self.gp_list[now_listIdx]['gp']

        self.round_int = lambda x: np.round((x * 2 + 1) // 2)

        c_x = int(Data.round_int(gp[0] * self.v_width))
        c_y = int(Data.round_int(gp[1] * self.v_height))

        return (c_x, c_y)


    round_int = lambda x: np.round((x * 2 + 1) // 2)


    def readLivedata(self) -> None:

        with gzip.open(self.f_path, 'rt') as f:
            f_text = f.read().split('\n')
            start_time = 0

            for line in f_text[:-1]:
                j_data = json.loads(line)

                if '"vts":0' in line:
                    start_time = j_data['ts']

                if 'gp' in line and 'gp3' not in line:
                    j_data['ts'] = j_data['ts'] - start_time
                    self.gp_list.append(j_data)
                    self.ts_list.append(j_data['ts'])


class Capture(cv2.VideoCapture):
    def __init__(self, f_path) -> None:
        super().__init__(f_path)
        self.write_frame = None
        self.width = int(self.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.height = int(self.get(cv2.CAP_PROP_FRAME_HEIGHT))
        self.fps = float(self.get(cv2.CAP_PROP_FPS))
        

    def mp4WriteConf(self, export_name, circle_color = (255,255,0), circle_line = -1, circle_size = 10) -> None:
        fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
        
        self.setCircleConf(circle_color, circle_line, circle_size)
        self.video = cv2.VideoWriter(export_name, fourcc, self.fps, (self.width, self.height))


    def setCircleConf(self, circle_color = (255,255,0), circle_line = -1, circle_size = 10) -> None:
        self.circle_color = circle_color
        self.circle_line = circle_line
        self.circle_size = circle_size


    def addcircle(self, frame, sight_xy):
        return cv2.circle(frame, sight_xy, self.circle_size, self.circle_color, thickness=self.circle_line)

    round_int = lambda x: np.round((x * 2 + 1) // 2)


if __name__ == '__main__':
    ## 自作モジュールの読み込み
    import tarminal

    ## コマンドの設定
    def setPath(arg, video_path, eye_data_path):
        """ファイルへのパス名を指定する"""
        return (arg[video_path], arg[eye_data_path])

    def setCircle(cap, color, thickness, size):
        """動画に描画する円の見た目を設定する"""
        cap.setCircleConf(color, thickness, size)

    cmd = tarminal.Command(sys.argv, {'-P':setPath, '-C':setCircle})

    arg_rule = [str]
    arg_exprain = ['出力動画ファイル名']
    arg_num = 1
    video_path = 'segments/1/fullstream.mp4'
    eye_data_path = 'segments/1/livedata.json.gz'
    export_path = 0
    color, thickness, size = (0,0,0)

    ## Pオプションの追加
    if cmd.isSetOption('-P'):
        arg_rule += [str, str]
        arg_exprain += ['入力動画ファイルへのパス','視線情報のgzipファイルへのパス']
        arg_num += 2
        video_path = arg_num - 2
        eye_data_path = arg_num - 1
        export_path = arg_num - 3

    ## Cオプションの追加
    if cmd.isSetOption('-C'):
        arg_rule += [str, int, int]
        arg_exprain += ['円の色"(B,G,R)"の形で0-255の値を指定','円の外周の太さpx(-1で塗りつぶし)','円の大きさpx']
        arg_num += 3
        color = arg_num - 3
        thickness = arg_num - 2
        size = arg_num - 1

    ## コマンドの実行
    cmd.setArgRule(arg_num, arg_rule, arg_exprain)
    arg = cmd.getArg()
    export_path = arg[0]

    try:
        video_path, eye_data_path = cmd.runOption('-P', (arg, video_path, eye_data_path))
    except:
        pass


    ## 動画の情報取得
    cap = Capture(video_path)

    ## 動画書き出し設定
    cap.mp4WriteConf(export_path)
    cmd.runOption('-C', (cap, eval(arg[color]), arg[thickness], arg[size]))

    ## プログレスバーの設定
    p_bar = tarminal.Progbar(int(cap.get(cv2.CAP_PROP_FRAME_COUNT)), message='Start Encoding.')

    ## 視線情報の取得
    eye = Data(eye_data_path, cap.width, cap.height)
    eye.readLivedata()

    while True:

        v_frame_now = cap.get(cv2.CAP_PROP_POS_FRAMES)
        v_time_now = cap.get(cv2.CAP_PROP_POS_MSEC)

        p_bar.view(v_frame_now)

        ret, frame = cap.read()

        if ret:
            sight_xy = eye.getSightPoint(v_time_now)

            im = cap.addcircle(frame, sight_xy)
                
            cap.video.write(im)

        else:
            break
    
    p_bar.progFinish('Finish Encoding.')

    cap.release()
    cv2.destroyAllWindows()