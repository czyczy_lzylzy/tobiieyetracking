import cv2
import numpy as np
import global_setting as glb

def getPtlist(im, wname):
    pt = []
    im_aoi = np.copy(im)
    cv2.namedWindow(wname)

    cv2.setMouseCallback(wname, addPoints, [wname, im_aoi, pt])
    cv2.imshow(wname, im_aoi)
    cv2.waitKey()
    cv2.destroyAllWindows()
    im_aoi = cv2.polylines(im, np.int32([pt]), True, (255,255,0))
    cv2.imshow(wname, im_aoi)
    cv2.waitKey(0)
    return pt, im_aoi

def addTrackPoint(im):
    wname = 'picPoints'
    v_points = []
    i_points = []

    frame = glb.resize.resize(glb.WIN_NAME, glb.frame)
    img = glb.resize.resize('im', im)

    cv2.namedWindow(wname)
    cv2.setMouseCallback(wname, picPoints, [wname, frame, glb.WIN_NAME, v_points, -1])
    cv2.imshow(wname, frame)
    cv2.waitKey()
    cv2.setMouseCallback(wname, picPoints, [wname, img, 'im', i_points, len(v_points)])
    cv2.imshow(wname, img)
    cv2.waitKey()
    cv2.destroyAllWindows()

    return i_points, v_points


def getArea(im, win_name):
    glb.points = []
    cv2.namedWindow(win_name)
    glb.im_area = np.copy(im)
    cv2.setMouseCallback(win_name, dragArea, win_name)
    cv2.imshow(win_name, glb.im_area)
    cv2.waitKey()


def addPoints(event, x, y, flag, params):
    wname, img, ptlist = params

    if event == cv2.EVENT_MOUSEMOVE:  
        img2 = np.copy(img)
        h, w = img2.shape[0], img2.shape[1]
        cv2.line(img2, (x, 0), (x, h - 1), (255, 0, 0))
        cv2.line(img2, (0, y), (w - 1, y), (255, 0, 0))
        cv2.imshow(wname, img2)

    if event == cv2.EVENT_LBUTTONDOWN:
        ptlist.append([x, y])
        cv2.circle(img, (x, y), 3, (0, 0, 255), 3)
        cv2.imshow(wname, img)

        if cv2.waitKey(1) == '\n':
            cv2.destroyWindow(wname)

def picPoints(event, x,y, flag, params):
    wname, img, mode, points, limit = params

    if event == cv2.EVENT_MOUSEMOVE:  
        im2 = np.copy(img)
        h, w = im2.shape[0], im2.shape[1]
        cv2.line(im2, (x, 0), (x, h - 1), (255, 0, 0))
        cv2.line(im2, (0, y), (w - 1, y), (255, 0, 0))
        cv2.imshow(wname, im2)

    if event == cv2.EVENT_LBUTTONDOWN:

        points.append(glb.resize.toBaseFromResizeXY(mode, [x, y]))
        cv2.circle(img, (x, y), 3, (0, 0, 255), 3)
        cv2.imshow(wname, img)

        if cv2.waitKey(1) == '\n':
            cv2.destroyWindow(wname)
    

def dragArea(event, x, y, flag, params, matches):
    wname = params

    if event == cv2.EVENT_MOUSEMOVE:
        if glb.drag:
            im_area = np.copy(glb.im_area)
            cv2.rectangle(im_area, tuple(glb.s_point_harf), (x,y), (255,0,0))
            cv2.imshow(wname, im_area)


    if event == cv2.EVENT_LBUTTONDOWN:
        if not glb.drag:
            glb.s_point = [x*2,y*2]
            glb.s_point_harf = [x,y]
            glb.drag = True
    
    if event == cv2.EVENT_LBUTTONUP:
        if glb.drag:
            glb.points.append([glb.s_point,[x*2,y*2]])
            cv2.rectangle(glb.im_area, tuple(glb.s_point_harf), (x,y), (255,0,0))
            glb.drag = False

    if cv2.waitKey(1) == '\n':
        cv2.destroyWindow(wname)
