from operator import imatmul, ipow, truediv
from types import coroutine
import cv2
import numpy as np
import sys
import os
import itertools
import joblib
from numpy.core.defchararray import array
import mouseManual as ms
import global_setting as glb
sys.path.append('mymodule')

class AOI():
    def __init__(self, name, i_points):
        self.name = name
        self.inFrame = False
        self.i_points = i_points
        self.v_points = None
        self.best_M = None
        self.best_ind = None
        self.sub_aoi = None
        self.v_match = None
        self.i_match = None
        self.track_points = None

    def loadData(self, aoi):
        self.name = aoi.name
        self.inFrame = aoi.inFrame
        self.i_points = aoi.i_points
        self.v_points = aoi.v_points
        self.best_M = aoi.best_M
        self.best_ind = aoi.best_ind
        self.sub_aoi = aoi.sub_aoi
        self.v_match = aoi.v_match
        self.i_match = aoi.i_match
        self.track_points = aoi.track_points

    def setImSize(self, width, height):
        self.im_width = width
        self.im_height = height
    
    def setVideSize(self, width, height):
        self.v_width = width
        self.v_height = height

    def addSubAOI(self, aoi):
        if self.sub_aoi is None:
            self.sub_aoi = dict()
        self.sub_aoi[aoi.name] = aoi

    def matching(self, v_im, im, detector, matcher, mask=None):
        v_kp, v_des = detector.detectAndCompute(v_im, None)
        i_kp, i_des = detector.detectAndCompute(im, mask)

        matches = matcher.knnMatch(v_des, i_des, k=2)
        return matches, v_kp, i_kp

    def addMatchPoints(self, i_match, v_match):
        self.v_match = np.float32(v_match)
        self.i_match = np.float32(i_match)
        self.best_ind = [0,1,2]
        self.track_points = self.v_match[self.best_ind]
        self.frameIN()


    ## アフィン変換の変換行列を生成、AOI座標を変換
    def affineAOI(self) -> None:
        if self.v_points is None:
            for i1, i2, i3 in getCombination(len(self.v_match)):
                index = [i1,i2,i3]
                # ang, isOkDistance = isNotNear3Points(self.i_match[index], self.im_width, self.im_height)
                # if isOkDistance:
                if isNotNear3Points(self.i_match[index]):
                    M = cv2.getAffineTransform(self.i_match[index], self.v_match[index])
                    points = self.homography(M) ## AOI座標を変換
                    self.setPtList(points, M, index, i_points=self.i_match[index])   ## 良い変換座標を選別

            ## サブAOIの変換
            if self.best_M is None:
                self.frameOUT()
            else:
                sub_homography(self, self.best_M['M'])

    def affineAOI_update(self, v_match, i_match) -> None:
        for i1, i2, i3 in getCombination(10 if len(v_match) > 10 else len(v_match)):
            index = [i1,i2,i3]

            if isNotNear3Points(i_match[index]):
                M = cv2.getAffineTransform(i_match[index], v_match[index])
                points = self.homography(M) ## AOI座標を変換
                if self.setPtList(points, M, index, i_points=i_match[index]):   ## 良い変換座標を選別
                    self.v_match = v_match
                    self.i_match = i_match

        ## サブAOIの変換
        if self.best_M is None:
            self.frameOUT()
        else:
            sub_homography(self, self.best_M['M'])


    def homography(self, M):
        new_ptList = []
        for p in self.i_points:
            new_ptList.append([dot(M, p)])
        return new_ptList

    def setPtList(self, new_points, M, index, i_points=None, update=False):
        ## 角度
        base = dot(M, [1,1])
        im_ang = angle(dot(M, [int(self.im_width), 1])-base, dot(M, [1, int(self.im_height)])-base)

        ## 変換後の角度
        if update:
            ## 面積
            if im_ang > 80 and im_ang < 100:
                self.best_M['im_ang'] = im_ang
                self.best_M['M'] = M
                self.v_points = new_points
                self.best_ind = index
                return True
        else:
            if im_ang > 80 and im_ang < 100:
                area = cv2.contourArea(i_points)
                area_norm = area / (self.v_width * self.v_height)
                points_norm = np.array(i_points) / np.tile([self.im_width, self.im_height],(3,1))
                score = (1-abs(np.corrcoef(points_norm.T)[0][1])) * area_norm

                if self.best_M is None or self.best_M['score'] < score:          
                    self.best_M = {'score':score, 'M':M, 'im_ang':im_ang}           
                    self.v_points = new_points
                    self.best_ind = index
                    return True
        return False


    ## AOIがフレームインしているかどうか
    def frameIN(self):
        self.inFrame = True
    def frameOUT(self):
        self.inFrame = False
    
    def update(self, points):
        self.v_points = points

    def reset(self):
        self.v_points = None
        self.best_M = None
        self.i_match = None
        self.v_match = None
        self.best_ind = None
        self.track_points = None
        self.frameOUT()

    ## 特徴点のトラッキング
    def tracking(self, prev_im, now_im) -> bool:
        if self.track_points is None:
            self.track_points = self.v_match[self.best_ind]
        points, status, err = cv2.calcOpticalFlowPyrLK(prev_im, now_im, np.float32(self.track_points), None)
        sucsess_p = np.sum(status)

        ## トラッキングが成功した場合
        if sucsess_p == 3:
            self.track_points = points
            
            M = cv2.getAffineTransform(np.float32(self.i_match[self.best_ind]), self.track_points)
            points = self.homography(M)
            
            ## AOI、サブAOIの変換
            if self.setPtList(points, M, self.best_ind, update=True):
                sub_homography(self, M)
                return True
  
        ## トラッキング失敗
        self.reset()
        return False

    def hitAOI(self, eye_point) -> any:
        hit_aoi = None
        if self.v_points is None or cv2.pointPolygonTest(np.array(self.v_points, dtype=np.int32), eye_point, measureDist=False) < 0:
            return hit_aoi
        else:
            hit_aoi = self.name

        if self.sub_aoi is not None:
            for sub in self.sub_aoi.values():
                if sub is None:
                    break
                hit = sub.hitAOI(eye_point)
                hit_aoi = hit if hit is not None else hit_aoi

        return hit_aoi



def sub_homography(aoi, M):
    if aoi.sub_aoi is not None:
        for sub in aoi.sub_aoi.values():
            if sub is None:
                break
            sub_homography(sub, M)
            points = sub.homography(M)
            sub.update(points)
    else:
        points = aoi.homography(M)
        aoi.update(points)


def commandMODE(cap):
    cmd = input('>>> ')
    order, prm = tuple(cmd.split(' '))
    
    if order == 'setframe':
        cap.set(cv2.CAP_PROP_POS_MSEC, float(prm))


def returnYesNo(question):
    print(question+' (y/n)')
    while True:
        answer = input('>>> ')
        if answer == 'y' or answer == 'n':
            return answer=='y'
    

def isNotNear3Points(points, w, h, threshold=4000):
    area = cv2.contourArea(points)
    v1 = np.array([0,w])
    v2 = np.array([h,0])
    ang1 = abs(angle(v1, v2))
    # return area/w*h, area/w*h > 0.1
    return abs(1-ang1 / 90), ang1 > 80 and ang1 < 100

def isNotNear3Points(points):
    return cv2.contourArea(points) > 4000


def inSquare(point, square):
    x = point[0]
    y = point[1]
    return x>=square[0][0] and x<=square[1][0] and y>=square[0][1] and y<=square[1][1]


## AOIの設定
def setAOI(im):
    if returnYesNo('保存したAOIを使用しますか'):
        print('使用するファイル名(variables下)を拡張子なしで入力してください')
        f_name = input('>>> ')
        aoi_dir_path = 'variables/aoi/'+f_name+'/'
        aoi = joblib.load(aoi_dir_path+f_name+'.jb')
        im_aoi = cv2.imread(aoi_dir_path+f_name+'.jpg')
        
    else:
        im_aoi = np.copy(im)
        aoi = inputAOI(im_aoi)

    return aoi, im_aoi

def inputAOI(im_aoi):
    print('AOIを入力し、Enterキーを押してください')
    print('AOI名を入力してください')
    aoi_name = input('>>> ')
    pt, im_aoi = ms.getPtlist(im_aoi, 'SetAOI')
    aoi = AOI(aoi_name, pt)

    while returnYesNo(aoi.name+'のサブAOIを設定しますか'):
        aoi.addSubAOI(inputAOI(im_aoi))

    return aoi

def saveAOI(aoi, im_aoi):
    if returnYesNo('設定したAOIを保存しますか'):
        print('保存するファイル名を拡張子なしで入力してください')
        f_name = input('>>> ')
        aoi_dir_path = 'variables/aoi/'+f_name+'/'
        os.makedirs(aoi_dir_path, exist_ok=True)
        joblib.dump(aoi, aoi_dir_path+f_name+'.jb')
        cv2.imwrite(aoi_dir_path+f_name+'.jpg', im_aoi)
        print('保存しました ({})'.format(aoi_dir_path+f_name+'.jb'))



## 検索画像にマスクを設定
def setMask(im):
    mask = None
    im_mask = None
    if returnYesNo('マスクを設定しますか'):
        if returnYesNo('保存したマスクを使用しますか'):
            print('使用するファイル名(variables下)を拡張子なしで入力してください')
            f_name = input('>>> ')
            mask_dir_path = 'variables/mask/'+f_name+'/'
            mask = joblib.load(mask_dir_path+f_name+'.jb')
            im_mask = cv2.imread(mask_dir_path+f_name+'.jpg')

        else:
            im_mask = np.copy(im)
            mask = inputMask(im_mask)

    return mask, im_mask

def inputMask(im_mask):
    win_name = 'SetMask'
    mask = np.full(im_mask.shape[:2], 0, dtype=np.bool8)
    while True:
        pt, _ = ms.getPtlist(im_mask, win_name)

        print('[i]: 内側塗りつぶし, [o]: 外側塗りつぶし')
        while True:
            io = input('>>> ')
            in_mask = np.full_like(im_mask, 0)
            in_mask = cv2.fillConvexPoly(in_mask, np.int32(pt), color=(255,255,255))
            one_mask = in_mask[:,:,0].astype(np.bool8)

            if io == 'i':
                mask = one_mask | mask
                im_mask[mask] = np.array([255,255,255], dtype=np.int32)
            elif io == 'o':
                mask = ~one_mask | mask
                im_mask[mask] = np.array([255,255,255], dtype=np.int32)
            else:
                continue
            cv2.imshow(win_name, im_mask)
            cv2.waitKey(0)
            break

        if not returnYesNo('引き続きマスクを設定しますか'):
            cv2.destroyAllWindows()
            return mask


def saveMask(mask, im_mask):
    if returnYesNo('設定したマスクを保存しますか'):
        print('保存するファイル名を拡張子なしで入力してください')
        f_name = input('>>> ')
        mask_dir_path = 'variables/mask/'+f_name+'/'
        os.makedirs(mask_dir_path, exist_ok=True)
        joblib.dump(mask, mask_dir_path+f_name+'_mask.jb')
        cv2.imwrite(mask_dir_path+f_name+'_mask.jpg', im_mask)
        print('保存しました ({})'.format(mask_dir_path+f_name+'_mask.jb'))



def setVideoConfig(fps, width, height):
    fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
    return cv2.VideoWriter(glb.EXPORT_NAME, fourcc, fps, (width, height))


## ベクトル間の角度を計算
def angle(v1, v2):
    return np.arccos(np.dot(v1,v2)/(np.linalg.norm(v1)*np.linalg.norm(v2)))*180/np.pi

## indexの組み合わせイテレータ生成
def getCombination(length):
    ind_list = [i for i in range(length)]
    return itertools.combinations(ind_list, 3)

## 変換行列を使って座標を変換する関数
def dot(M:np.array, point:list):
    _point = np.float32(point+[1])
    return np.dot(M, _point)[:2].astype(int)


## マッチした特徴点同士のベクトルの角度をもとに外れ値を除外
def dropOutliers(v_match, i_match, dis_list):
    if len(v_match) >=3:
        match_arr = np.array(v_match) - np.array(i_match)
        norm = [np.linalg.norm(v) for v in match_arr]
        base_arr = np.array([1,1])
        angle_arr = angle(match_arr, base_arr)
        ang_median = np.median(angle_arr)
        drop_bool = ~(np.array(angle_arr > ang_median*1.05) | np.array(angle_arr < ang_median*0.95))
        return v_match[drop_bool], i_match[drop_bool], np.array(dis_list)[drop_bool]
    
    else:
        return [], [], []


def aoiMatching(aoi, frame, im, detector, matcher, ratio=0.7, mask=None):
    ## 特徴量マッチング
    matches, v_kp, i_kp = aoi.matching(frame, im, detector, matcher, mask=mask)
 
    v_match = []
    i_match = []
    dis_list = []

    ## レシオテスト
    for m, n in matches:
        if m.distance < ratio * n.distance:
            dis_list.append(m.distance)
            v_match.append(list(v_kp[m.queryIdx].pt))
            i_match.append(list(i_kp[m.trainIdx].pt))

    ## 外れ値を除去
    # v_match, i_match, dis_list = dropOutliers(np.float32(v_match), np.float32(i_match), dis_list)
    v_match = np.float32(v_match)
    i_match = np.float32(i_match)
    dis_list = np.array(dis_list)

    if len(v_match) >= 4:
        aoi.frameIN()
        aoi.v_match = sortUpper(v_match, dis_list)
        aoi.i_match = sortUpper(i_match, dis_list)
    else:
        aoi.frameOUT()

    if aoi.inFrame:
        ## 変換行列を生成、AOI座標の変換
        aoi.affineAOI()
    else:
        aoi.reset()

def updateMatching(aoi, frame, im, detector, matcher, ratio=0.7, mask=None):
    ## 特徴量マッチング
    matches, v_kp, i_kp = aoi.matching(frame, im, detector, matcher, mask=mask)
 
    v_match = []
    i_match = []
    dis_list = []

    ## レシオテスト
    for m, n in matches:
        if m.distance < ratio * n.distance:
            dis_list.append(m.distance)
            v_match.append(list(v_kp[m.queryIdx].pt))
            i_match.append(list(i_kp[m.trainIdx].pt))

    ## 外れ値を除去
    # v_match, i_match, dis_list = dropOutliers(np.float32(v_match), np.float32(i_match), dis_list)
    v_match = np.float32(v_match)
    i_match = np.float32(i_match)
    dis_list = np.array(dis_list)

    if len(v_match) >= 4:
        v_match = sortUpper(v_match, dis_list)
        i_match = sortUpper(i_match, dis_list)
        aoi.affineAOI_update(v_match, i_match)



## 配列をbase_listの昇順に並び替える
def sortUpper(arr, base_list):
    smaller_ind = np.argsort(base_list)
    return arr[smaller_ind]

def exportAOI(aoi, v_im):
    if aoi.sub_aoi is not None:
        for sub in aoi.sub_aoi.values():
            if sub is None:
                break
            v_im = exportAOI(sub, v_im)

    return cv2.polylines(v_im, np.int32([aoi.v_points]), True, (255,255,0))


class VideoQueue():
    def __init__(self, num) -> None:
        self.q = []
        self.len = num

    def push(self, item) -> any:
        self.q.append(item)
        return self.pop(0) if len(self.q) == self.len else None

    def pop(self, num) -> any:
        return self.q.pop(num)

    def rewind(self) -> bool:
        if len(self.q) > 1:
            del self.q[-1]
            return True
        else:
            return False

    def view(self, num) -> any:
        return self.q[num]

    def update(self, item):
        try:
            self.q[-1] = item
        except IndexError:
            self.q.append(item)