import numpy as np
import cv2
import sys
import aoiMod
sys.path.append('mymodule')
import imModule


def init():
    global drag
    global DIR_PATH
    global SEARCH_IM
    global EXPORT_NAME
    global WIN_NAME
    global STOP_FILE
    global detector
    global matcher
    global frame_q
    global aoi_q
    global prev_aoi
    global s_time
    global export_data
    global points
    global s_point
    global s_point_harf
    global v_frame_now
    global v_time_now
    global frame
    global prev_im
    global resize
    global ratio
    global back_ground


    EYE_DATA_DIR_NAME = 'nrjum2m'
    SEARCH_IM = 'trackingImage/test03.jpg'
    EXPORT_NAME = 'export/export_video_sae_single.mp4'
    STOP_FILE = 'stopFile/stop_sae_single.csv' ## filename or None
    ratio = 0.6

    DIR_PATH = 'eyeData/' + EYE_DATA_DIR_NAME + '/segments/1/'
    WIN_NAME = 'AOI Tracker'

    # detector = cv2.AKAZE_create()  ## A-KAZE検出器
    # detector = cv2.xfeatures2d.SURF_create() ## SURF検出器
    detector = cv2.ORB_create() ## ORB検出器
    matcher = cv2.BFMatcher()  ## Brute-Force Matcher
    # matcher = cv2.FlannBasedMatcher()

    resize = imModule.Resize()

    drag = False
    frame_q = aoiMod.VideoQueue(300)
    aoi_q = aoiMod.VideoQueue(300)
    prev_aoi = 'out'
    s_time = 0
    export_data = []
    points = []
    s_point = None
    s_point_harf = None
    v_frame_now = 0
    v_time_now = 0
    frame = None
    prev_im = None
    back_ground = True