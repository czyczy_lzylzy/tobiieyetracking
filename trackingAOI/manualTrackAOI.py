import cv2
import numpy as np
import sys
import pandas as pd
import copy
import itertools
import joblib
import mouseManual as ms
import global_setting as glb
sys.path.append('mymodule')
import aoiMod as amod
import tobiiUnpack as tobii
import tarminal


def keyMenu(key, aoi, cap, im):
    w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    ## escキーでプログラム終了
    if key == 27:
        print('Esc key pressed')
        print('Finish this program...')
        cv2.destroyAllWindows()
        sys.exit()

    ## spaceキーで一時停止
    if key == 32:
        key = cv2.waitKey(0)
        
        while key != 32:
            glb.v_frame_now = cap.get(cv2.CAP_PROP_POS_FRAMES)
            glb.v_time_now = cap.get(cv2.CAP_PROP_POS_MSEC)
            
            ## コマンドモード
            if key == ord('c'):
                amod.commandMODE(cap)

            ## <- 1フレーム巻き戻し
            elif key == ord('b'):
                if glb.frame_q.rewind():
                    cap.set(cv2.CAP_PROP_POS_FRAMES, glb.v_frame_now-2)

                    glb.prev_im = np.copy(glb.frame)
                    ret, glb.frame = cap.read()
                    glb.v_frame_now = cap.get(cv2.CAP_PROP_POS_FRAMES)
                    glb.v_time_now = cap.get(cv2.CAP_PROP_POS_MSEC)

                    glb.aoi_q.rewind()
                    _, dat, aoi_name = glb.aoi_q.view(-1)
                    aoi.loadData(dat)

                    view_im = glb.frame_q.view(-1)
                    
                    cv2.imshow(glb.WIN_NAME, view_im)

            ## -> 1フレーム進む
            elif key == ord('n'):
                ret, glb.frame = cap.read()
                glb.v_frame_now = cap.get(cv2.CAP_PROP_POS_FRAMES)
                glb.v_time_now = cap.get(cv2.CAP_PROP_POS_MSEC)

                if ret:
                    ## aoiのトラッキング
                    if aoi.inFrame:
                        sucsess = aoi.tracking(glb.prev_im, glb.frame)
                        if sucsess:
                            ## AOI hitとフレーム画像をキューにプッシュ
                            write_data = glb.aoi_q.push((glb.v_time_now, copy.deepcopy(aoi), aoi.hitAOI(eye.getSightPoint(glb.v_time_now))))
                            ## AOI hitデータの前処理
                            dataPreprocessing(write_data)
                        else:
                            aoi.reset()
                    ## 情報を追加した画像を生成
                    test_im = makeIm()
                    test_im = glb.resize.resize(glb.WIN_NAME, test_im)
                    write_im = glb.frame_q.push(test_im)

                    ## フレーム画像を動画に出力
                    if write_im is not None:
                        video.write(write_im)
                
                    glb.prev_im = glb.frame

                else:
                    break

                cv2.imshow(glb.WIN_NAME, glb.resize.resize(glb.WIN_NAME, makeIm()))

            ## トラッキングする範囲の指定
            elif key == ord('a'):
                matches, v_kp, i_kp = aoi.matching(glb.frame, im)
                ## 探索範囲の指定
                i_h, i_w, _ = im.shape
                ms.getArea(glb.resize.resize(glb.WIN_NAME, im), 'Traking Area', matches, i_kp)
                aoi.selectPoints(matches, v_kp, i_kp)

                if aoi.inFrame:
                    ## 変換行列を生成、AOI座標の変換
                    aoi.affineAOI(glb.prev_im, glb.frame)
                else:
                    aoi.reset()

            ## トラッキングする特徴点の追加
            elif key == ord('p'):
                i_points, v_points = ms.addTrackPoint(im)
                aoi.addMatchPoints(i_points, v_points)

                ## AOIを変換
                M = cv2.getAffineTransform(np.float32(aoi.i_match[aoi.best_ind]), aoi.track_points)
                points = aoi.homography(M)
                aoi.setPtList(points, M, aoi.best_ind, i_points=np.array(i_points, dtype=np.int32))

                ## サブAOIの変換
                amod.sub_homography(aoi, M)
                
                ## 情報を追加した画像を生成
                test_im = makeIm()
                test_im = glb.resize.resize(glb.WIN_NAME, test_im)
                cv2.imshow(glb.WIN_NAME, test_im)

                glb.prev_im = glb.frame
                glb.aoi_q.update((glb.v_time_now, copy.deepcopy(aoi), aoi.hitAOI(eye.getSightPoint(glb.v_time_now))))
                glb.frame_q.update(test_im)


            ## AOIの検出、トラッキング
            elif key == ord('s'):
                aoi.reset()
                if not aoi.inFrame:
                    amod.aoiMatching(aoi, glb.prev_im, glb.frame, im, glb.detector, glb.matcher)
                else:
                    sucsess = aoi.tracking(glb.prev_im, glb.frame)
                    
                    if not sucsess:
                        amod.aoiMatching(aoi, glb.prev_im, glb.frame, im, glb.detector, glb.matcher)
                        
                if aoi.inFrame:
                    ## AOI hitとフレーム画像をキューにプッシュ
                    glb.aoi_q.update((glb.v_time_now, copy.deepcopy(aoi), aoi.hitAOI(eye.getSightPoint(glb.v_time_now))))

                cv2.imshow(glb.WIN_NAME, glb.resize.resize(glb.WIN_NAME, makeIm()))

            ## AOIを動かす
            elif key == ord('m'):
                pass

            ## リセット
            if key == ord('r'):
                aoi.reset()

            ## プログラム終了
            elif key == 27:
                cv2.destroyAllWindows()
                sys.exit()

            ## バックグラウンドに移行
            elif key == ord('g'):
                cv2.destroyAllWindows()
                glb.back_ground = True
                return

            key = cv2.waitKey(0)

    ## リセット
    if key == ord('r'):
        aoi.reset()


def dataPreprocessing(data:tuple):
    if data is None:
        return

    time, _, aoi_name = data

    if aoi_name is None:
        if glb.prev_aoi != 'out':
            glb.export_data.append([glb.s_time/1000, time, glb.prev_aoi])
            glb.prev_aoi = 'out'
        
    elif aoi_name != glb.prev_aoi:
        if glb.prev_aoi != 'out':
            glb.export_data.append([glb.s_time/1000, time, glb.prev_aoi])
        glb.prev_aoi = aoi_name
        glb.s_time = time
    
    ## AOI hitをファイル出力
    df = pd.DataFrame(glb.export_data)
    df.to_csv('aoi_hit.csv', header=False, index=False)


def makeIm():
    ## AOI情報を追加した映像を表示
    test_im = np.copy(glb.frame)
    
    ## AOIの枠を画像に追加
    if aoi.v_points is not None:
        test_im = amod.exportAOI(aoi, cv2.polylines(test_im, np.int32([aoi.v_points]), True, (255,255,0)))
    
    ## トラッキングしている特徴点を画像に追加
    if aoi.track_points is not None:
        for p in aoi.track_points:
            test_im = cv2.circle(test_im, tuple(map(int, p)), 5, (0,255,0), thickness=-1)

    ## 注視点を画像に追加
    test_im = cv2.circle(test_im, eye.getSightPoint(glb.v_time_now), 10, (15,241,255), thickness=-1)
    ## 経過時間を追加
    test_im = cv2.putText(test_im, str(glb.v_time_now), (0,50), cv2.FONT_HERSHEY_PLAIN, 3, (255, 255, 255), 5, cv2.LINE_AA)

    return test_im


if __name__ == '__main__':

    glb.init()

    ## STOPファイルの読み込み
    if glb.STOP_FILE != None:
        stop_time_iter = iter(np.loadtxt(glb.STOP_FILE))
        stop_time = next(stop_time_iter)

    ## 動画の情報を取得
    cap = cv2.VideoCapture(glb.DIR_PATH+'fullstream.mp4')

    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = float(cap.get(cv2.CAP_PROP_FPS))
    all_frame = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    ## 目標画像の情報を取得
    im = cv2.imread(glb.SEARCH_IM)


    ## AOIの設定
    aoi, im_aoi = amod.setAOI(im)
    amod.saveAOI(aoi, im_aoi)
    aoi.setImSize(width, height)

    ## 視線情報ファイルの読み込み
    eye = tobii.Data(glb.DIR_PATH+'livedata.json.gz', width, height)
    eye.readLivedata()

    glb.prev_im = None

    ## 1フレーム目の情報を取得
    ret, glb.frame = cap.read()
    glb.v_frame_now = cap.get(cv2.CAP_PROP_POS_FRAMES)
    glb.v_time_now = cap.get(cv2.CAP_PROP_POS_MSEC)

    height, width, _ =  glb.frame.shape
    aoi.setVideSize(width, height)

    ## リサイズの設定
    glb.resize.addResizeMode(glb.WIN_NAME, glb.frame, 0.5)
    glb.resize.addResizeMode('im', im, 0.5)

    ## 動画書き出し用の設定
    video = amod.setVideoConfig(fps, int(width*glb.resize.getBairitsu(glb.WIN_NAME)), int(height*glb.resize.getBairitsu(glb.WIN_NAME)))

    ## プログレスバーの設定
    p_bar = tarminal.Progbar(int(cap.get(cv2.CAP_PROP_FRAME_COUNT)), message='Start Encoding.')

    while True:
        ## プログレスバーの表示
        p_bar.view(glb.v_frame_now)

        if ret:

            ## aoiのトラッキング
            if aoi.inFrame:
                sucsess = aoi.tracking(glb.prev_im, glb.frame)
                if sucsess:
                    pass
                else:
                    aoi.reset()


            ## 情報を追加した画像を生成
            test_im = makeIm()
            test_im = glb.resize.resize(glb.WIN_NAME, test_im)
            if not glb.back_ground:
                cv2.imshow(glb.WIN_NAME, test_im)


            ## AOI hitとフレーム画像をキューにプッシュ
            write_data = glb.aoi_q.push((glb.v_time_now, copy.deepcopy(aoi), aoi.hitAOI(eye.getSightPoint(glb.v_time_now))))
            ## AOI hitデータの前処理
            dataPreprocessing(write_data)

            write_im = glb.frame_q.push(test_im)

            ## フレーム画像を動画に出力
            if write_im is not None:
                video.write(write_im)
        
            glb.prev_im = glb.frame

        else:
            break

        key = cv2.waitKey(1)

        ## STOPファイルでのストップ処理
        if glb.STOP_FILE != None and stop_time == glb.v_time_now:
            try:
                stop_time = next(stop_time_iter)
            except:
                glb.STOP_FILE = None
            key = 32
            glb.back_ground = False
            cv2.imshow(glb.WIN_NAME, test_im)

        keyMenu(key, aoi, cap, im)

        ret, glb.frame = cap.read()
        glb.v_frame_now = cap.get(cv2.CAP_PROP_POS_FRAMES)
        glb.v_time_now = cap.get(cv2.CAP_PROP_POS_MSEC)

    p_bar.progFinish('Finish Encoding.')

    ## 残りのデータを前処理
    for d in glb.aoi_q.q:
        dataPreprocessing(d)

    ## 残りのフレームを動画に出力
    for f in glb.frame_q.q:
        video.write(f)
        
    ## AOI hitをファイル出力
    df = pd.DataFrame(glb.export_data)
    df.to_csv('aoi_hit.csv', header=False, index=False)
