import cv2

inFile = 'auto_export_video_ORB06.mp4'
forFPS = 25.01
outFile = inFile.replace('.mp4','_'+str(forFPS)+'.mp4')


class Progbar():

    def __init__(self, works, now=0, bar_len=30, icon='#', message='Start.'):
        self.works    = works
        self.now      = now
        self.bar_len  = bar_len
        self.finish   = False
        self.icon     = icon
        self.icon_num = 0
        self.blank_num = bar_len
        self.prog_par = int(self.now/self.works)
        self.par_prev = -1
        self.bar_update = True
        print(message)

    def setIconNum(self):
        self.par_prev = self.prog_par
        self.prog_par = int(self.now/self.works*100)
        self.icon_num = int(self.now/self.works * self.bar_len) + 1
        self.blank_num = self.bar_len - self.icon_num
        self.bar_update = self.prog_par != self.par_prev

    def progress(self, num, add_mode):
        self.now = self.now+num if add_mode else num

    def view(self, num, add_mode=False):
        self.progress(num, add_mode)
        self.setIconNum()
        if self.bar_update:
            print('\r['+ self.icon*self.icon_num + ' '*self.blank_num +']', end='')
            print(' '+str(self.prog_par)+'%  ', end='')

    def progFinish(self, message='Finish.'):
        print('\n'+message)


if __name__ == '__main__':
    cap = cv2.VideoCapture(inFile)

    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = float(cap.get(cv2.CAP_PROP_FPS))
    all_frame = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
    video = cv2.VideoWriter(outFile, fourcc, forFPS, (width, height))

    print('input File: '+inFile)
    print('output File: '+outFile)
    print('FPS: '+ str(fps) + '->' + str(forFPS))

    p_bar = Progbar(all_frame)

    while True:

        ret, frame = cap.read()
        if ret:

            now_frame = int(cap.get(cv2.CAP_PROP_POS_FRAMES))
            p_bar.view(now_frame)
            video.write(frame)

        else:
            break

    p_bar.progFinish()